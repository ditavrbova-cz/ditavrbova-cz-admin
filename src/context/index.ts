import { Credentials } from "../types/auth.ts";
import { createContext, useContext } from "react";

export type ApplicationContextState = {
    credentials: Credentials | null;
    setCredentials: (credentials: Credentials | null) => void;
};

export const ApplicationContext = createContext<ApplicationContextState | null>(null);

export const useApplicationContext = (): ApplicationContextState => {
    const context = useContext(ApplicationContext);

    if (context === null) {
        throw "ApplicationContext is null!";
    }

    return context as ApplicationContextState;
};