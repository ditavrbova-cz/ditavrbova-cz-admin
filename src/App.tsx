import { NextUIProvider } from "@nextui-org/react";
import { FC, useEffect, useState } from "react";
import { ApplicationContext } from "./context";
import { Credentials, initializeAuthentication, storeAuthenticationCredentials } from "./types/auth.ts";
import { Login } from "./components/Login.tsx";
import { Administration } from "./components/Administration.tsx";

const App: FC = () => {
    const [credentials, setCredentials] = useState<Credentials | null>(null);
    const setCredentialsPersisting = (value: Credentials) => {
        storeAuthenticationCredentials(value);
        setCredentials(value);
    };

    useEffect(() => void initializeAuthentication(setCredentials), []);

    return (
        <NextUIProvider>
            <ApplicationContext.Provider value={{credentials, setCredentials: setCredentialsPersisting}}>
                {
                    credentials === null
                        ? <Login/>
                        : <Administration/>
                }
            </ApplicationContext.Provider>
        </NextUIProvider>
    );
};

export default App;