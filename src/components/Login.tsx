import { ChangeEvent, FC, KeyboardEvent, useState } from "react";
import { Modal, ModalContent, ModalHeader, ModalBody, ModalFooter, Button, Input } from "@nextui-org/react";
import { Eye, EyeOff } from "react-feather";
import { Credentials } from "../types/auth.ts";
import { checkCredentials } from "../api/api.ts";
import { useApplicationContext } from "../context";

export const Login: FC = () => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);

    const valid = username.trim().length > 0 && password.trim().length > 0;

    const { setCredentials } = useApplicationContext();

    const submit = async () => {
        if (!valid) {
            return;
        }

        setLoading(true);

        const credentials: Credentials = { username, password };
        const authenticated = await checkCredentials(credentials);

        setLoading(false);

        if (!authenticated) {
            setError(true);
            return;
        }

        setCredentials(credentials);
    };

    const handleKeydown = (event: KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter" && valid) {
            submit().catch(console.error);
        }
    }

    const handleChange = (event: ChangeEvent<HTMLInputElement>, setter: (value: string) => void) => {
        setError(false);
        setter(event.target.value);
    }

    const [passwordVisible, setPasswordVisible] = useState(false);
    const togglePasswordVisibility = () => {
        setPasswordVisible(current => !current);
    };

    return (
        <>
            <Modal isOpen={true} closeButton={<></>}>
                <ModalContent>
                    <>
                        <ModalHeader className="flex flex-col gap-1">Přihlášení do administrace</ModalHeader>
                        <ModalBody>
                            <Input label="Uživatelské jméno"
                                   type="text"
                                   value={username}
                                   onChange={(event) => handleChange(event, setUsername)}
                                   onKeyDown={handleKeydown}
                                   validationState={error ? "invalid" : "valid"}
                                   isDisabled={loading}
                            />
                            <Input label="Heslo"
                                   type={passwordVisible ? "text" : "password"}
                                   value={password}
                                   onChange={(event) => handleChange(event, setPassword)}
                                   onKeyDown={handleKeydown}
                                   isDisabled={loading}
                                   validationState={error ? "invalid" : "valid"}
                                   endContent={
                                       <button className="focus:outline-none" type="button" onClick={togglePasswordVisibility} tabIndex={-1}>
                                           {
                                               passwordVisible
                                                   ? <Eye className="w-4 h-4 opacity-50"/>
                                                   : <EyeOff className="w-4 h-4 opacity-50"/>
                                           }
                                       </button>
                                   }
                            />
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary" onPress={() => submit()} isDisabled={!valid} isLoading={loading}>
                                Pokračovat
                            </Button>
                        </ModalFooter>
                    </>
                </ModalContent>
            </Modal>
        </>
    );
}