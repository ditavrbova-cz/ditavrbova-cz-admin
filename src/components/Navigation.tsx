import { FC } from "react";
import { Link as RouterLink } from "react-router-dom";
import { Navbar, NavbarBrand, NavbarContent, NavbarItem, NavbarMenuToggle } from "@nextui-org/react";
import { useApplicationContext } from "../context";

export const Navigation: FC = () => {
    const {setCredentials} = useApplicationContext();

    return (
        <Navbar>
            <NavbarContent className="flex-grow-0">
                <NavbarMenuToggle className="sm:hidden"/>
                <NavbarBrand className="font-black">
                    Administrace
                </NavbarBrand>
            </NavbarContent>
            <NavbarContent className="hidden sm:flex gap-4 flex-grow" justify="center">
                <NavbarItem>
                    <RouterLink to="/products" className="text-sm sm:text-xs text-neutral-600 font-bold">Produkty</RouterLink>
                </NavbarItem>
                <NavbarItem>
                    <RouterLink to="/" className="text-sm sm:text-xs text-neutral-600 font-bold" onClick={() => setCredentials(null)}>Odhlásit se</RouterLink>
                </NavbarItem>
            </NavbarContent>
        </Navbar>
    );

}