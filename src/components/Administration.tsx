import { FC } from "react";
import { createHashRouter, Outlet, redirect, Route, RouterProvider, Routes } from "react-router-dom";
import { Dashboard } from "./pages/Dashboard.tsx";
import { Navigation } from "./Navigation.tsx";

const Layout: FC = () => {
    return (
        <>
            <Navigation/>
            <div className="container mx-auto mt-4">
                <Outlet/>
            </div>
        </>
    );
};

const Root: FC = () => {
    return (
        <Routes>
            <Route element={<Layout/>}>
                <Route path="/" element={<Dashboard/>}/>
                <Route path="*" action={() => redirect("/")}/>
            </Route>
        </Routes>
    );
};

const router = createHashRouter([
    {
        path: "*",
        element: <Root/>
    }
]);

export const Administration: FC = () => {
    return (
        <>
            <RouterProvider router={router}/>
        </>
    )

};
