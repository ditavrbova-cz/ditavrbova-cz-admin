import { Credentials } from "../types/auth.ts";

const API_BASE = "https://api.ditavrbova.cz/api/v1";

const authentication = (request: RequestInit, credentials: Credentials): RequestInit => {
    const token = `${credentials.username}:${credentials.password}`;
    const header = `Basic ${btoa(token)}`;

    return {
        ...request,
        headers: {
            ...request.headers,
            "Authorization": header
        }
    }
}

const json = (request: RequestInit, payload: unknown): RequestInit => {
    const body = JSON.stringify(payload);

    return {
        ...request,
        body,
        method: "post",
        headers: {
            ...request.headers,
            "Content-Type": "application/json",
        }
    };
}


export const checkCredentials = async (credentials: Credentials): Promise<boolean> => {
    try {
        const response = await fetch(`${API_BASE}/authentication/check-password`, authentication({ method: "post" }, credentials));
        return response.ok;
    }
    catch (error) {
        console.error(error);
        return false;
    }
}