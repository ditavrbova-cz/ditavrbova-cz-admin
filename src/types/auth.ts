export type Credentials = {
    username: string;
    password: string;
};

export const initializeAuthentication = (setCredentials: (credentials: Credentials) => void) => {
    const storedCredentials = window.localStorage.getItem("storedCredentials");

    if (!storedCredentials) {
        return;
    }

    setCredentials(JSON.parse(storedCredentials));
};

export const storeAuthenticationCredentials = (credentials: Credentials) => {
    window.localStorage.setItem("storedCredentials", JSON.stringify(credentials));
};